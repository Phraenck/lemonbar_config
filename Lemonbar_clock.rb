require 'date'
require_relative 'Lemonbar_base.rb'

#display a clock
class Lemonbar_clock
  include Lemonbar_base

  @@dayhash = {
    '0' => '日',
    '1' => '月',
    '2' => '火',
    '3' => '水',
    '4' => '木',
    '5' => '金',
    '6' => '土'
  }

  def objinit
    #do nothing
  end

  def render
    weekday = DateTime.now.strftime("%w")
    timestr = DateTime.now.strftime("  %Y.%m.%d (#{@@dayhash[weekday]}) %H%M  ")
    @value = background(getAccent(), foreground($FG_COLOR, timestr))
  end
end
