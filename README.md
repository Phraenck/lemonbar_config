My lemonbar (https://github.com/LemonBoy/bar) configuration for use with bspwm (https://github.com/baskerville/bspwm).

Supports showing the active workspace, which workspaces have open windows, the current weather, the date, and the time.